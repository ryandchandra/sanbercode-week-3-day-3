@extends('adminlte.master')

@section('content')
    <div class="mt-6 ml-6">
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">Index</h3>
            </div>
            <div class="card-body">
            <a href="/pertanyaan/create" class="btn btn-primary">Tambah</a>
            <br><br>
            <table class="table table-bordered">
                <thead class="thead-light">
                <tr>
                    <th scope="col">No</th>
                    <th scope="col">Judul</th>
                    <th scope="col">Isi</th>
                    <th scope="col">Actions</th>
                </tr>
                </thead>
                <tbody>
                    @forelse ($pertanyaan as $key=>$value)
                        <tr>
                            <td>{{$key + 1}}</th>
                            <td>{{$value->judul}}</td>
                            <td>{{$value->isi}}</td>
                            <td>
                                <a href="/pertanyaan/{{$value->id}}" class="btn btn-info">Show</a>
                                <a href="/pertanyaan/{{$value->id}}/edit" class="btn btn-primary">Edit</a>
                                <form action="/pertanyaan/{{$value->id}}" method="POST" style="display:inline">
                                    @csrf
                                    @method('DELETE')
                                    <input type="submit" class="btn btn-danger my-1" value="Delete">
                                </form>
                            </td>
                        </tr>
                    @empty
                        <tr colspan="3">
                            <td>No data</td>
                        </tr>  
                    @endforelse              
                </tbody>
            </table>
            </div>
        </div>
    </div>
@endsection