@extends('adminlte.master')

@section('content')
    <div class="mt-6 ml-6">
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">Edit</h3>
            </div>
            <div class="card-body">
            <div>
                <h2>Edit Post {{$pertanyaan->id}}</h2>
                <form action="/pertanyaan/{{$pertanyaan->id}}" method="POST">
                    @csrf
                    @method('PUT')
                    <div class="form-group">
                        <label for="judul">Judul</label>
                        <input type="text" class="form-control" name="judul" value="{{$pertanyaan->judul}}" id="judul" placeholder="Masukkan Judul">
                        @error('judul')
                            <div class="alert alert-danger">
                                {{ $message }}
                            </div>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="isi">body</label>
                        <input type="text" class="form-control" name="isi"  value="{{$pertanyaan->isi}}"  id="isi" placeholder="Masukkan Isi">
                        @error('body')
                            <div class="alert alert-danger">
                                {{ $message }}
                            </div>
                        @enderror
                    </div>
                    <button type="submit" class="btn btn-primary">Edit</button>
                </form>
            </div>
            </div>
        </div>
    </div>
@endsection