@extends('adminlte.master')

@section('content')
    <div class="mt-6 ml-6">
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">Show</h3>
            </div>
            <div class="card-body">
                <h2>Show Post {{$pertanyaan->id}}</h2>
                <h4>{{$pertanyaan->judul}}</h4>
                <p>{{$pertanyaan->isi}}</p>

                <a href="/pertanyaan" class="btn btn-primary">Back</a>
            </div>
        </div>
    </div>
@endsection